package demo.repository.library;

import org.springframework.data.jpa.repository.JpaRepository;

import demo.entities.LibraryEntity;

public interface LibraryRepository extends JpaRepository<LibraryEntity	, Long>{

}
