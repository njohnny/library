package demo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;

import demo.entities.LibraryEntity;

import demo.service.LibraryService;

@org.springframework.web.bind.annotation.RestController
@RequestMapping("/api/library")
public class RestController {
	
	@Autowired
	public LibraryService libraryService;
	
	@RequestMapping(value="/librarys", method = RequestMethod.GET)
	@ResponseStatus(HttpStatus.OK)
	public List<LibraryEntity> getLibrarys(){
		
		List<LibraryEntity> librarys = this.libraryService.getLibrarys();
		
		return librarys;
	}
	
	@RequestMapping(value="/create", method = RequestMethod.POST)
	@ResponseStatus(HttpStatus.CREATED)
	public void createLibrary(@RequestBody LibraryEntity library) {
		
		this.libraryService.createLibrary(library);
	}
	
	@RequestMapping(value="/update", method = RequestMethod.PUT)
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void updateLibrary(@RequestBody LibraryEntity library) {
		
		this.libraryService.updateLibrary(library);
	}
	
	@RequestMapping(value = "/delete", method = RequestMethod.DELETE)
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void deleteLibrary(@RequestBody LibraryEntity library) {
		
		this.libraryService.deleteLibrary(library);
	}

}
