package demo.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import demo.entities.LibraryEntity;
import demo.repository.library.LibraryRepository;

@Service
public class LibraryService {
	
	@Autowired
	private LibraryRepository repository;

	public List<LibraryEntity> getLibrarys() {
		
		return this.repository.findAll();
	}

	public void createLibrary(LibraryEntity library) {
		
		
		this.repository.save(library);
	}

	public void updateLibrary(LibraryEntity library) {
		
		Long id = library.getId();
	 	LibraryEntity libraryFound = this.repository.findById(id).get();
	 	
	 	if(libraryFound != null) {
	 		libraryFound.setNome(library.getNome());
	 		this.repository.save(libraryFound);
	 	}
		
	}

	public void deleteLibrary(LibraryEntity library) {
		
		Long id = library.getId();
	 	LibraryEntity libraryFound = this.repository.findById(id).get();
	 	
	 	if(libraryFound != null) {
	 		this.repository.delete(libraryFound);
	 	}
	}

}
